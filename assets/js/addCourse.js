let courseContainer = document.querySelector("#courseContainer")

courseContainer.addEventListener("submit" , (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let courseDescription = document.querySelector("#courseDescription").value
	let coursePrice = document.querySelector("#coursePrice").value
	
	if((courseName !== '' && courseDescription !== '' && coursePrice !== '')) {
	
		fetch('http://localhost:4000/api/users/course-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
				},
				body: JSON.stringify({    
					courseName: courseName
				})
			})
		.then(res => res.json())
		.then(data => {
			
			if(data === false){
				fetch('http://localhost:4000/api/users/courses', {
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						coursename: coursename,
						courseDescription: courseDescription,
						coursePrice: coursePrice,
					})
				})
				.then(res => { 
					return res.json()
				})
				.then(data => {
					console.log(data)
				
					if(data === true){
						alert("New course successfully created"),
						window.location.replace("./courses.html")
					} else {
						alert("Something went wrong in adding a course")
					}
				})
			}
		})

	} else {
		alert("Something went wrong, check your credentials")
	}
})

