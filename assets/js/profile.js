const token = localStorage.getItem('token')
console.log(token)

let profileContainer = document.querySelector('#profileContainer')

if (!token || token === null) {
 		alert("You must login first")
 		window.location.href = "./login.html"

} else {
	fetch('http://localhost:4000/api/users/details', {
 		headers: {
 			Authorization: `Bearer ${token}`
 		}
 	})
 		.then(res => res.json())
    	.then(data => {
    		console.log(data)
            enrollmentData = data.enrollments.map(classData => {
            	console.log(classData)
            	return(
	            	`
					<tr>
					   <td>${classData.courseId}</td> 
					   <td>${classData.enrolledOn}</td> 
					   <td>${classData.status}</td> 
					</tr> 
					`
            	)
            }).join('')
            	profileContainer.innerHTML = 
            	`<div class="col-md-12">
					<section class="jumbotron my-5">
						<h3 class="text-center">
							First Name: ${data.firstName}
						</h3>
						<h3 class="text-center">
							Last Name: ${data.lastName}
						</h3>
						<h3 class="text-center">
							Email: ${data.email}
						</h3>
						<h3 class="text-center">
							Class History
						</h3>
						<table class="table">
							<thead>
								<tr>
									<th> CourseID </th>
									<th> Enrolled On </th>
									<th> Status </th>
								</tr>
								<tbody>
									${enrollmentData}
								</tbody>
							</thead>
						</table>
					</section>
				</div>`
        })
    }
