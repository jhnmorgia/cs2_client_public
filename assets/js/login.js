let loginForm = document.querySelector("#loginUser") //get the loginUser ID from front end

loginForm = loginForm.addEventListener("submit", (e) => { //this script is for the loginForm to listen to the function
    e.preventDefault() //prevent default is for the function to run once

    let email = document.querySelector ("#userEmail").value  //to get the input value of the email
    let password = document.querySelector ("#password").value //to get the input value of the password

    if(email == '' || password == '') {   //if email and/or password is blank
        alert("Please input email and/or password")   //it will alert a message "please input email and/or password"
    } else {   //else if the email and input have both values
        fetch('http://localhost:4000/api/users/login', {    //this will fetch the login page
            method: 'POST',  //and will bost the details below on the page
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify ({   //the values of email and password will be converted to string
                email: email,
                password: password
            })
        })
        .then(res => {  //then "promise" that the result will return 
            return res.json()
        })
        .then(data => {
            console.log(data)
            if(data.access){
                //store the JWT inside locaStorage
                localStorage.setItem('token', data.access)
                fetch('http://localhost:4000/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    localStorage.setItem('id',data._id)
                    localStorage.setItem('isAdmin',data.isAdmin)
                    window.location.replace("./profile.html")
                })                   
            } else {
                alert("Something went wrong, check your credentials")
            }
        })
    }
})


