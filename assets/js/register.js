let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit" , (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let email = document.querySelector("#userEmail").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value
	
	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {
	
		fetch('http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
				},
				body: JSON.stringify({    
					email: email
				})
			})
		.then(res => res.json())
		.then(data => {
			
			if(data === false){
				fetch('http://localhost:4000/api/users', {
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => { 
					return res.json()
				})
				.then(data => {
					console.log(data)
				
					if(data === true){
						alert("New account has registered successfully"),
			
						window.location.replace("./login.html")
					} else {
						alert("Something went wrong in the registration")
					}
				})
			}
		})

	} else {
		alert("Something went wrong, check your credentials")
	}
})

