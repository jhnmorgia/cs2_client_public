let _courseId = sessionStorage.getItem("courseId")
let _isAdmin = localStorage.getItem("isAdmin")
let _userId = localStorage.getItem("id")
const token = localStorage.getItem("token")
let appendable = document.getElementById("appendable");

if(_courseId !== null){
	if(_isAdmin === 'true'){
		fetch('http://localhost:4000/api/courses/${courseId}', {
			method: "GET",
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => {
			return res.json()
		})
		.then(course => {
			let courseName = document.getElementById("courseName")
			let courseDescription = document.getElementById("courseDescription")
			let coursePrice = document.getElementById("coursePrice")

			courseName.innerHTML = course.courseName
			courseDescription.innerHTML = course.courseDescription
			coursePrice.innerHTML = course.coursePrice

			let isChecked = "";

			if(course.isActive) isChecked = "checked";

			let adminAccess =
			`
			<div class="col-md-8">
				<section class="jumbotron my-5">
					<h2 class="text-dark">ENROLLED STUDENTS</h2>
						<table class="table table-dark"
							<thead>
								<tr>
									<th class="text-center">First Name</th>
									<th class="text-center">Last Name</th>
									<th class="text-center">E-mail</th>
								</tr>
								<tr>
									<td class="text-center" id="studentFName"></td>	
									<td class="text-center" id="studentLName"></td>	
									<td class="text-center" id="studentEmail"></td>
								</tr>
							</thead>
						</table>
				</section>
			</div>					
			`

			appendable.innerHTML = adminAccess;

			let archiveBtn = document.querySelector("#archiveBtn");
			let courseIsActive = document.getElementById("courseIsActive");

			archiveBtn.addEventListener("click", () => {
				if(archiveBtn.checked === true){
					fetch('http://localhost:4000/api/courses/${courseId}', {
						method: "PUT",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						},
						body: JSON.stringify({
							isActive:true
						})
					})
					.then(res => {
						return res.json()
					})
					courseIsActive.innerHTML = "Course Status: Active"
				} else {
					fetch('http://localhost:4000/api/courses/${courseId}', {
						method: "DELETE",
						headers: {
							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`
						}
					})
					.then(res => {
						return res.json()
					})
					courseIsActive.innerHTML = "Course Status: INACTIVE"
				}
			})
		})
	} else {
		fetch('http://localhost:3000/api/courses/${_courseId}', {
			
		})
	}
}
